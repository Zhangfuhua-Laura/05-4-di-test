package com.twuc.webApp.domain.mazeRender;

/**
 * 迷宫渲染网格的联通方向。可以按位进行组合。
 */
public final class Direction {
    /**
     * 当前网格是一个孤立的网格。
     */
    public static final int UNKNOWN = 0;

    /**
     * 当前网格和东方的同类网格联通。
     */
    public static final int EAST = 1;

    /**
     * 当前网格和西方的同类网格联通。
     */
    public static final int WEST = 2;

    /**
     * 当前网格和北方的同类网格联通。
     */
    public static final int NORTH = 4;

    /**
     * 当前网格和南方的同类网格联通。
     */
    public static final int SOUTH = 8;
}

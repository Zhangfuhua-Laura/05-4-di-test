package com.twuc.webApp.domain.mazeRender;

import java.util.List;

/**
 * 渲染迷宫所需要的各个组成部分的抽象工厂。
 */
public interface MazeComponentFactory {

    /**
     * 获得该迷宫的背景渲染器。这些渲染器将按顺序执行渲染操作。
     *
     * @return 背景渲染器列表。
     */
    List<AreaRender> createBackgroundRenders();

    /**
     * 获得该迷宫的墙体渲染器。这些渲染器将按照顺序执行渲染操作。
     *
     * @return 墙体渲染器列表。
     */
    List<CellRender> createWallRenders();

    /**
     * 获得该迷宫的道路渲染器。这些渲染器将按照顺序执行渲染操作。
     *
     * @return 道路渲染器列表。
     */
    List<CellRender> createGroundRenders();

    /**
     * 获得该迷宫渲染的基本参数。
     *
     * @return 迷宫渲染参数。
     */
    MazeRenderSettings createSettings();
}

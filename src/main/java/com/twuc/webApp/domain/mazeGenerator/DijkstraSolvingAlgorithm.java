package com.twuc.webApp.domain.mazeGenerator;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * 计算从一个指定点走到迷宫中的路径中的任意点的最小步数。这个算法会将步数写入 {@link Grid} 中 {@link GridCell}
 * 的 tag 中。
 */
@Component
public class DijkstraSolvingAlgorithm implements MazeUpdater {
    private static final String PriorityNodeKey = "priority_node";

    /**
     * 用于获得从指定点到当前点的最小步数的 tag key。
     */
    public static final String ResolvingDistance = "resolve_distance";
    private final int startRow;
    private final int startColumn;

    /**
     * 创建一个迷宫路径更新算法。以左上角作为起点。
     */
    public DijkstraSolvingAlgorithm() {
        this(0, 0);
    }

    private DijkstraSolvingAlgorithm(int startRow, int startColumn) {
        if (startRow < 0) { throw new IllegalArgumentException("Invalid start row index."); }
        if (startColumn < 0) { throw new IllegalArgumentException("Invalid start column index."); }
        this.startRow = startRow;
        this.startColumn = startColumn;
    }

    @Override
    public void update(Grid grid) {
        if (startRow >= grid.getRowCount() || startColumn >= grid.getColumnCount()) {
            throw new IllegalArgumentException("The grid is too small to hold source cell index");
        }

        GridCell sourceCell = grid.getCell(startRow, startColumn);
        sourceCell.getTags().put(PriorityNodeKey, createPriorityNode(sourceCell, 0));

        for (GridCell cell :
            Arrays.stream(grid.getCells()).filter(cell -> !sourceCell.equals(cell)).toArray(GridCell[]::new))
        {
            PriorityQueueNode priorityQueueNode = createPriorityNode(cell, Integer.MAX_VALUE);
            cell.getTags().put(PriorityNodeKey, priorityQueueNode);
        }

        PriorityQueue<PriorityQueueNode> priorityQueue = new PriorityQueue<>(grid.size());
        for (GridCell cell : grid.getCells())
        {
            PriorityQueueNode cellTag = getPriorityNodeOnCell(cell);
            priorityQueue.add(cellTag);
        }

        while (priorityQueue.size() > 0)
        {
            PriorityQueueNode minDistanceNode = priorityQueue.remove();
            GridCell minDistanceCell = minDistanceNode.getCurrentCell();
            for (GridCell neighbor : minDistanceCell.getLinks())
            {
                int altDistance = minDistanceNode.getPriority() + 1;
                PriorityQueueNode neighborNode = getPriorityNodeOnCell(neighbor);
                if (altDistance < neighborNode.getPriority())
                {
                    neighborNode.setPreviousCell(minDistanceCell);
                    updatePriority(priorityQueue, neighborNode, altDistance);
                }
            }
        }

        for (GridCell cell : grid.getCells())
        {
            PriorityQueueNode node = getPriorityNodeOnCell(cell);
            cell.getTags().remove(PriorityNodeKey);
            cell.getTags().put(ResolvingDistance, node.getPriority());
        }
    }

    private void updatePriority(
        PriorityQueue<PriorityQueueNode> priorityQueue, PriorityQueueNode neighborNode, int altDistance) {
        if (!priorityQueue.remove(neighborNode)) {
            throw new RuntimeException("Corrupted queue!");
        }

        neighborNode.setPriority(altDistance);
        priorityQueue.add(neighborNode);
    }

    private PriorityQueueNode getPriorityNodeOnCell(GridCell cell) {
        return (PriorityQueueNode)cell.getTags().get(PriorityNodeKey);
    }

    private PriorityQueueNode createPriorityNode(GridCell cell, int distance) {
        return new PriorityQueueNode(distance, cell, null);
    }
}

